$(function(){
	$('a[href^=#]').click(function() {
	  var speed = 400; // ミリ秒
	  var href= $(this).attr("href");
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  var position = target.offset().top;
	$('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
	});
});

$(document).ready(function(){
    $(".gotop").hide();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() > 100) {
            $(".gotop").fadeIn("fast");
        } else {
            $(".gotop").fadeOut("fast");
        }
        scrollHeight = $(document).height(); //ドキュメントの高さ 
        scrollPosition = $(window).height() + $(window).scrollTop(); //現在地 
        footHeight = $("footer").innerHeight() - 50; //footerの高さ（＝止めたい位置）
        if ( scrollHeight - scrollPosition  <= footHeight ) { //ドキュメントの高さと現在地の差がfooterの高さ以下になったら
            $(".gotop").css({
                "position":"absolute", //pisitionをabsolute（親：wrapperからの絶対値）に変更
                "bottom": footHeight + 25  //下からfooterの高さ + 25px上げた位置に配置
            });
        } else { //それ以外の場合は
            $(".gotop").css({
                "position":"fixed", //固定表示
                "bottom": "25px" //下から25px上げた位置に
            });
        }
    });
});

$(function(){
	// スマホ、タブレットでタッチしたらクラスを付与
	$( '.tap')
		.bind('touchstart', function(){
			$(this).addClass('ontap');
			}).bind('touchend', function(){
			$(this).removeClass('ontap');
	});

//	// 表示中のページのメニューにクラスを付与
//	var target = null;
//	$('.subnav li a').each(function(){
//		var $href = encodeURI($(this).attr('href'));
//		if(location.href.match($href)) {
//			//$(this).addClass('active');
//			target = $(this);
//		} else {
//			//$(this).removeClass('active');
//		}
//	});
//	if (target) {
//		target.addClass('active');
//	}
	
});

//Youtube動画のコントロール
$(function() {
	$('#gochi_video').on('click', function() {
		if (!$(this).attr('checked')) {
			console.log('success');
			videoControl('stopVideo');
		}
		function videoControl(action){
			var $playerWindow = $('#popup-youtube-player')[0].contentWindow;
			$playerWindow.postMessage('{"event":"command","func":"'+action+'","args":""}', '*');
		}
	});
});
