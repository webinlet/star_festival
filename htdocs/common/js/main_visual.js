/**
 * メインビジュアルJS
 *
 * @author Tsue Shogo
 * @version
 *	1.0.0	2018/08/29(水) 11:59	新規作成
 *	1.1.0	2018/08/29(水) 18:04	left条件微修正
 *	1.2.0	2018/08/29(水) 20:38	クローン追加
 *	1.3.0	2018/08/30(木) 16:51	タイマー追加
 *	1.4.0	2018/09/10(月) 17:25	アニメションの処理を選択可能に
 *	2.0.0	2018/09/12(水) 13:08	IE9以上に対応
 *	2.0.1	2018/09/13(木) 13:41	head内に置くと不具合が起きる件修正
 *	2.0.2	2018/09/13(木) 14:52	IE9-console.logエラーが起きるのでコメントアウト
 *	2.0.3	2018/09/14(金) 09:28	IE9,10でopaciyボタンが効かなくなる件修正
 *	2.1.0	2018/10/12(金) 13:10	見た目変更。白箱無しで黒文字から白文字に変化
 */
(function(){
	/**
	 * コントローラ
	 *
	 * @var Object
	 */
	var Controller = {
		/**
		 * パラメータ
		 *
		 * @var Object
		 */
		params: {
			// シーンID
			scene: 0,
			// アニメーションタイプ
			animateType: 'time',	// time: 時間処理, frame: フレーム処理, auto: IEの場合はframeにそれ以外の場合はtimeに
			// スライド幅と画像幅の割合
			maskRatio: 0.7,
			// スライド内の画像の進行方向
			maskDirection: 'left',	// right: 左→右, left: 右→左
			// スライダーのスピード[px/s]
			sliderSpeed: 150,
			// シーン2から3に切り替わる時間[ms]
			duration2to3: 500,
			// クローン数
			cloneCnt: 3,
			// クローン前のセル数
			cellCntBare: 0,
			// サイズ配列
			maskWidth: [],
			maskHeight: [],
			imageWidth: [],
			imageHeight: [],
			nativeImageWidth: [],
			nativeImageHeight: [],
			// left配列
			maskLefts: [],
			// サイクル幅
			cycleWidth: 0,
			// 1フレーム前のサイクル幅
			tmpCycleWidth: 0,
			// window幅
			windowWidth: 0,
			// シーン2から3に切り替わっているか
			status2to3: false,
			// UserAgent
			ua: null,
			// 開発モード
			debug: false
		},
		/**
		 * イベント
		 *
		 * @var Object
		 */
		events: {
			'#top_kv .stop_btn': {
				'click': 'clickStopBtn'
			},
			'window': {
				'resize': 'resizeWindow'
			}
		},
		/**
		 * 初期実行
		 *
		 * @return void
		 */
		init: function(){
			var _this = this;
			// 開発モード
			if (this.params.debug) {
				var $debug = $('<div id="debug"></div>');
				$debug.css({'position': 'fixed', 'right': 0, 'top': 0, 'z-index': 10000});
				$('body').append($debug);
			}
			
			// UA判別
			this.params.ua = new UserAgent();
			if (this.params.animateType === 'auto') {
				if (this.params.ua.isIE) {
					this.params.animateType = 'frame';
					this.params.sliderSpeed = 300;
				} else {
					this.params.animateType = 'time';
				}
			}
			//console.log('ua=' + navigator.userAgent.toLowerCase());
			//console.log('animateType=' + this.params.animateType);
			
			// クローン前のセル数取得
			this.params.cellCntBare = $('#top_kv .slider .slide_top li').length;
			
			// 自然長取得
			this.getNativeSize();
			
			// クローン作成
			var $clone = $('#top_kv .slider .slide_top').clone();
			$('#top_kv .slider .slide_top li').addClass('bare');
			for (var i = 1; i < this.params.cloneCnt; i++) {
				$('#top_kv .slider .slide_top').append($clone.html());
			}
			
			// シーン1に変更
			var promise = this.changeScene(1);
			
			// シーン2に変更
			$.when(promise).done(function(){
				_this.changeScene(2);
			});
		},
		/**
		 * [event] resizeWindow
		 *
		 * @param object	event
		 * @param object	target
		 */
		resizeWindow: function(event, target) {
			this.updateSliderSize();
		},
		/**
		 * [event] ストップボタンをクリック
		 *
		 * @param object	event
		 * @param object	target
		 */
		clickStopBtn: function(event, target) {
			// シーン2のときのみ動作する
			if (this.params.scene !== 2) {
				return;
			}
			// シーン3に変更
			this.changeScene(3);
		},
		/**
		 * 自然長取得
		 *
		 * @return void
		 */
		getNativeSize: function() {
			var _this = this;
			var $target = $('#top_kv .slide_top');
			$('li img.ani_slide', $target).each(function(index){
				_this.params.nativeImageWidth[index] = $(this).attr('width') * 1;
				_this.params.nativeImageHeight[index] = $(this).attr('height') * 1;
			});
		},
		/**
		 * スライダーサイズ変更
		 *
		 * @return void
		 */
		updateSliderSize: function() {
			var _this = this;
			var $target = $('#top_kv .slide_top');
			// オリジナルのセルのみでループ
			this.params.tmpCycleWidth = this.params.cycleWidth;
			this.params.cycleWidth = 0;
			$('li.bare', $target).each(function(index){
				// サイズ再計算
				var maskHeight = $target.height();
				var imageHeight = maskHeight;
				var imageWidth = imageHeight * _this.params.nativeImageWidth[index] / _this.params.nativeImageHeight[index];
				var maskWidth = imageWidth * _this.params.maskRatio;
				_this.params.maskWidth[index] = maskWidth;
				_this.params.maskHeight[index] = maskHeight;
				_this.params.imageWidth[index] = imageWidth;
				_this.params.imageHeight[index] = imageHeight;
				// サイクル幅
				_this.params.cycleWidth += maskWidth;
			});
			// 全てのセルでループ
			var left = 0;
			$('li', $target).each(function(index){
				var bareIndex = index % _this.params.cellCntBare;
				// left計算
				_this.params.maskLefts[index] = left;
				// 変更
				$(this).width(_this.params.maskWidth[bareIndex]);
				$(this).height(_this.params.maskHeight[bareIndex]);
				$(this).css({'left': left + 'px'});
				$('.ani_slide', this).width(_this.params.imageWidth[bareIndex]);
				$('.ani_slide', this).height(_this.params.imageHeight[bareIndex]);
				left += _this.params.maskWidth[bareIndex];
			});
			// window幅更新
			this.params.windowWidth = $(window).width();
		},
		/**
		 * スライダー開始
		 *
		 * @return void
		 */
		startSlider: function() {
			var _this = this;
			// 設定値
			var t = 0;
			var $target = $('#top_kv .slide_top');
			// スライダーサイズ更新
			this.updateSliderSize();
			// 開始
			var timer = new Timer();
			timer.set('start');
			timer.set('current');
			var flag = true;
			function frame() {
				// スライダーleft
				var sliderLeft = -_this.params.sliderSpeed * t / 1000;
				while (sliderLeft < -_this.params.cycleWidth) {
					sliderLeft += _this.params.cycleWidth;
				}
				$target.css({'left': sliderLeft + 'px'});
				// 2→3への移行中の場合
				if (_this.params.status2to3) {
					// 現在の位相を確認(位相：sliderLeftは0→-Lと変化するが、これを0→1と見立てる)
					var imaginTheta = -sliderLeft / _this.params.tmpCycleWidth;
					var newT = -sliderLeft / _this.params.tmpCycleWidth / _this.params.sliderSpeed * 1000 * _this.params.cycleWidth;
					switch (_this.params.animateType) {
						case 'time':
							timer._memory['start'] -= newT - t;
							break;
						case 'frame':
							t = newT;
							break;
						default:
					}
				}
				// 画像マージン計算
				var windowWidth = _this.params.windowWidth;
				var marginRatio = 1 - _this.params.maskRatio;
				switch (_this.params.maskDirection) { // ※計算量をなるべく減らすため先にswitch
					case 'right': // 左→右
						$('li', $target).each(function(index){
							// 計算
							var bareIndex = index % _this.params.cellCntBare;
							var maskX = _this.params.maskLefts[index] + sliderLeft;
							var imageWidth = _this.params.imageWidth[index];
							if (maskX > windowWidth) {
								margin = 0;
							} else if (maskX > - imageWidth) {
								margin = - imageWidth * marginRatio / (windowWidth + imageWidth) * (windowWidth - maskX);
							} else {
								margin = - imageWidth * marginRatio;
							}
							// 更新
							$('.ani_slide', this).css({'margin-left': margin});
						});
						break;
					case 'left':
						$('li', $target).each(function(index){
							// 計算
							var bareIndex = index % _this.params.cellCntBare;
							var maskX = _this.params.maskLefts[index] + sliderLeft;
							var imageWidth = _this.params.imageWidth[bareIndex];
							if (maskX > windowWidth) {
								margin = - imageWidth * marginRatio;
							} else if (maskX > - imageWidth) {
								margin = - imageWidth * marginRatio / (windowWidth + imageWidth) * (imageWidth + maskX);
							} else {
								margin = 0;
							}
							// 更新
							$('.ani_slide', this).css({'margin-left': margin});
						});
						break;
					default:
				}
				// t更新
				switch (_this.params.animateType) {
					case 'time':
						t = timer.get('start');
						break;
					case 'frame':
						t += timer.delay;
						break;
					default:
				}
				// 開発モード
				if (_this.params.debug) {
					var diff = timer.get('current');
					timer.set('current');
					$('#debug').html(diff);
				}
				// 次フレーム実行
				if (flag) {
					timer.frame(frame);
				}
			}
			frame();
		},
		/**
		 * シーンを変更する
		 *
		 * @param int	id
		 * @return Promise
		 */
		changeScene: function(id) {
			var _this = this;
			var deferred = new $.Deferred();
			switch (this.params.scene = id) {
				case 1:
					// class変更
					$('#top_kv').removeClass('scene2').removeClass('scene3').addClass('scene1');
					// windowスクロールバー非表示
					$('html').css({'overflow': 'hidden', 'position': 'fixed'});
					$(window).on('touchmove.noScroll', function(e){
						e.preventDefault();
					}, {passive: false});
					// アニメーション
					$.when(_this.animate($('.top_kv_intro div.line1'), 'fly-in')).done(function(){
						$.when(_this.animate($('.top_kv_intro div.line2'), 'fly-in')).done(function(){
							$.when(_this.animate($('.top_kv_intro div.line3'), 'fly-in')).done(function(){
								$.when(_this.animate($('.top_kv_intro div.line4'), 'fly-in')).done(function(){
									deferred.resolve();
								});
							});
						});
					});
					break;
				case 2:
					$('body,html').scrollTop(0);
					// class変更
					$('#top_kv').removeClass('scene1').removeClass('scene3').addClass('scene2');
					// bgアニメーション
					this.animate($('.top_kv_intro_bg'), 'smaller');
					// lineアニメーション
					$('.top_kv_intro .black').animate({'opacity': 0}, 1000);
					$('.top_kv_intro .white').animate({'opacity': 1}, 1000);
					// stopボタン表示
					$('#top_kv .stop_btn').fadeIn();
					if (this.params.ua.isIE9 || this.params.ua.isIE10) {
						$('#top_kv #stop_btn').css({'background-color': '#fff', 'opacity': '0'});
					}
					// スライダーオン
					this.startSlider();
					break;
				case 3:
					// stopボタン非表示
					$('#top_kv .stop_btn').fadeOut();
					// 現在のサイズ取得
					var $target = $('#top_kv .kv');
					var startWidth = $target.width();
					var startHeight = $target.height();
					var startLeft = 0;
					var startTop = 0;
					// class変更
					$('#top_kv').removeClass('scene1').removeClass('scene2').addClass('scene3');
					// ※メモ: CSSが効いた状態の値を取得しようとしたがIE9だと動作が不安定になるので固定値で指定する
					//var nativeWidth = $target.width();
					//var nativeHeight = $target.height();
					//var nativeLeft = $target.css('margin-left').split('px').join('') * 1;
					//var nativeTop = $target.css('margin-top').split('px').join('') * 1;
					if (_this.params.windowWidth > 768) {
						// PC
						var nativeWidth = startWidth - 100;
						var nativeHeight = 600;
						var nativeLeft = 50;
						var nativeTop = 50;
					} else {
						// SP
						var nativeWidth = startWidth - 100;
						var nativeHeight = 500;
						var nativeLeft = 30;
						var nativeTop = 30;
					}
					// アニメーション開始
					this.params.status2to3 = true;
					var thisDeferred = new $.Deferred();
					var thisPromise = thisDeferred.promise();
					$target.width(startWidth)
					$target.height(startHeight);
					$target.css({'margin-left': startLeft, 'margin-top': startTop});
					$target.animate(
						{
							'width': nativeWidth,
							'height': nativeHeight,
							'margin-left': nativeLeft,
							'margin-top': nativeTop,
						},
						this.params.duration2to3,
						'linear',
						function() {
							// 更新
							_this.params.status2to3 = false;
							$target.removeAttr('style');
							$('html').removeAttr('style');
							$(window).off('.noScroll');
							// 終了
							thisDeferred.resolve();
						}
					);
					var timer = new Timer();
					var flag = true;
					var frame = function() {
						// スライダーサイズ更新
						_this.updateSliderSize();
						// 次のフレーム
						if (flag) {
							timer.frame(frame);
						}
					}
					$.when(thisPromise).done(function(){
						flag = false;
					});
					frame();
					break;
				default:
			}
			return deferred.promise();
		},
		/**
		 * アニメーション処理(もともとCSSでやっていたものの移植)
		 *
		 * @param Object $target
		 * @param string key
		 * @return Promise
		 */
		animate: function($target, key) {
			var deferred = new $.Deferred();
			switch (key) {
				case 'fly-in':
					$target.css({'left': '-2em', 'opacity': 0});
					$target.animate({'left': 0, 'opacity': 1}, 1000, 'linear', function(){
						deferred.resolve();
					});
					break;
				case 'smaller':
					$target.animate({'opacity': 0}, 500, 'linear', function(){
						$target.width(218);
						$target.height(156);
						//$target.css({'box-shadow': '1px 1px 2px 0 rgba(0,0,0,0.2)'});
						$target.animate({'opacity': 0}, 500, 'linear', function(){
							deferred.resolve();
						});
					});
					break;
				case 'scean3':
					$target.css({'padding': 0});
					$target.animate({'padding': '50px'}, 500, 'linear', function(){
						deferred.resolve();
					});
					break;
				case 'scean3_sp':
					$target.css({'padding': 0});
					$target.animate({'padding': '30px 20px'}, 500, 'linear', function(){
						deferred.resolve();
					});
					break;
				default:
			}
			return deferred.promise();
		}
	};
	/**
	 * タイマー
	 *
	 * @return Object
	 */
	var Timer = function() {
		return {
			/**
			 * 現在の時刻ミリ秒
			 *
			 * @return int
			 */
			now: function() {
				if (false && typeof(performance) !== 'undefined' && typeof(performance.now) !== 'undefined') {
					return performance.now();
				} else {
					var d = new Date();
					return d.getTime();
				}
			},
			/**
			 * セッター
			 *
			 * @param string tag
			 * @return int
			 */
			set: function(tag) {
				return this._memory[tag] = this.now();
			},
			/**
			 * ゲッター
			 *
			 * @param string fromTag
			 * @param string toTag
			 * @return int
			 */
			get: function(fromTag, toTag) {
				var to = (typeof(toTag) === 'undefined' || typeof(this._memory[toTag]) === 'undefined') ? this.now() : this._memory[toTag];
				var from = (typeof(fromTag) === 'undefined' || typeof(this._memory[fromTag]) === 'undefined') ? this.now() : this._memory[fromTag];
				return to - from;
			},
			/**
			 * メモリ
			 *
			 * @var Object
			 */
			_memory: {},
			/**
			 * アニメーションフレーム
			 *
			 * @param function func
			 * @return void
			 */
			frame: function(func) {
				if (typeof(window.requestAnimationFrame) !== 'undefined') {
					window.requestAnimationFrame(func);
				} else {
					window.setTimeout(func, this.delay);
				}
			},
			/**
			 * 遅延
			 *
			 * @var number
			 */
			delay: 1000 / 60
		};
	};
	/**
	 * UserAgent情報
	 *
	 * @var Object
	 */
	var UserAgent = function() {
		var ua = navigator.userAgent.toLowerCase();
		var ver = navigator.appVersion.toLowerCase();
		
		// IE(11以外)
		var isMSIE = (ua.indexOf('msie') > -1) && (ua.indexOf('opera') == -1);
		// IE6
		var isIE6 = isMSIE && (ver.indexOf('msie 6.') > -1);
		// IE7
		var isIE7 = isMSIE && (ver.indexOf('msie 7.') > -1);
		// IE8
		var isIE8 = isMSIE && (ver.indexOf('msie 8.') > -1);
		// IE9
		var isIE9 = isMSIE && (ver.indexOf('msie 9.') > -1);
		// IE10
		var isIE10 = isMSIE && (ver.indexOf('msie 10.') > -1);
		// IE11
		var isIE11 = (ua.indexOf('trident/7') > -1);
		// IE
		var isIE = isMSIE || isIE11;
		// Edge
		var isEdge = (ua.indexOf('edge') > -1);
		
		// Google Chrome
		var isChrome = (ua.indexOf('chrome') > -1) && (ua.indexOf('edge') == -1);
		// Firefox
		var isFirefox = (ua.indexOf('firefox') > -1);
		// Safari
		var isSafari = (ua.indexOf('safari') > -1) && (ua.indexOf('chrome') == -1);
		// Opera
		var isOpera = (ua.indexOf('opera') > -1);
		
		return {
			isMSIE: isMSIE,
			isIE6: isIE6,
			isIE7: isIE7,
			isIE8: isIE8,
			isIE9: isIE9,
			isIE10: isIE10,
			isIE11: isIE11,
			isIE: isIE,
			isEdge: isEdge,
			isChrome: isChrome,
			isFirefox: isFirefox,
			isSafari: isSafari,
			isOpera: isOpera
		};
	};
	jQuery(function($){
		// 初期実行
		Controller.init();
		// イベント登録
		Object.keys(Controller.events).forEach(function(selector){
			var events = Controller.events[selector];
			Object.keys(events).forEach(function(event){
				var func = events[event];
				if (selector !== 'window') {
					$(document).on(event, selector, function(eve){
						Controller[func](eve, this);
					});
				} else {
					$(window).on(event, function(eve){
						Controller[func](eve, this);
					});
				}
			});
		});
	});
})();